#!/usr/bin/env python

import sys
import rospy
import json
from dt_position.srv import *

def add_two_ints_client(x, y, z, ox, oy, oz, ow):
    rospy.wait_for_service('add_two_ints')
    try:
        add_two_ints = rospy.ServiceProxy('add_two_ints', Service)
        print "here"
        (resp1, resp2, resp3, resp4, resp5, resp6, resp7) = add_two_ints(x, y, z, ox, oy, oz, ow)
        print "repo1 {0:4} repo2 {1} repo3 {2} repo4 {3} repo5 {4} repo6 {5} repo7 {6}".format(resp1, resp2, resp3, resp4, resp5, resp6, resp7)

        #return (resp1.a, resp1.b) 
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def usage():
    return "%s [x y]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 8:
        x = int(sys.argv[1])
        y = int(sys.argv[2])
        z = int(sys.argv[3])
        ox = int(sys.argv[4])
        oy = int(sys.argv[5])
        oz = int(sys.argv[6])
        ow = int(sys.argv[7])

    else:
        #print usage()
        sys.exit(1)
    print "Requesting %s %s"%(x, y)
    add_two_ints_client(x, y, z, ox, oy, oz, ow)
    #print "Requesting %s+%s"%(x, y)
    #print "%s + %s = %s"%(x, y, add_two_ints_client(x, y))
