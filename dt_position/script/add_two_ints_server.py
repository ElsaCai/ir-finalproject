#!/usr/bin/env python

from dt_position.srv import Service,ServiceResponse
import rospy
import json
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
"""
def handle_add_two_ints(req):
    print "Returning [%s + %s = %s]"%(req.a, req.b, (req.a + req.b))
    return ServiceResponse(req.a + req.b)
"""

global_x = 0
global_y = 0
global_z = 0
global_ox = 0
global_oy = 0
global_oz = 0
global_ow = 0

def call_position_value():
    print "global_x {0:4} global_y {1:4} global_z {2:4}".format(global_x, global_y, global_z)
    print "global_ox {0:4} global_oy {1:4} global_oz {2:4} global_ow {3:4}".format(global_ox, global_oy, global_oz, global_ow)
 
    return (global_x, global_y)

def get_position(req):
    global global_x
    global global_y
    global_x = req.a/100.0
    global_y = req.b/100.0
    global_z = req.c/100.0
    global_ox = req.d/100.0
    global_oy = req.e/100.0
    global_oz = req.f/100.0
    global_ow = req.g/100.0

    client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    client.wait_for_server()

    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = "map"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position.x = global_x
    goal.target_pose.pose.position.y = global_y
    goal.target_pose.pose.orientation.z = global_oz
    goal.target_pose.pose.orientation.w = global_ow

    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server not available!")
        rospy.signal_shutdown("Action server not available!")
    else:
        return client.get_result()

    call_position_value()
    print "X = %f, Y = %f, Z = %f"%(global_x, global_y, global_z)
    print "OX = %f, OY = %f, OZ = %f, OW = %f"%(global_ox, global_oy, global_oz, global_ow)

    return (req.a, req.b, req.c, req.d, req.e, req.f, req.g)

def add_two_ints_server():
    rospy.init_node('add_two_ints_server')
    rospy.Service('add_two_ints', Service, get_position)
    print "Ready to add two ints."
    rospy.spin()

if __name__ == "__main__":
    add_two_ints_server()
