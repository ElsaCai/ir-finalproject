from geometry_msgs.msg import Pose, PoseArray, Quaternion
from pf_base import PFLocaliserBase
import math
import rospy

from util import rotateQuaternion, getHeading
from random import random

from time import time

import numpy.ma as ma

import numpy as np
import random
import math

import time

import copy

import amcl_kld
class PFLocaliser(PFLocaliserBase):
       
    def __init__(self):
        # ----- Call the superclass constructor
        super(PFLocaliser, self).__init__()
        
        # ----- Set motion model parameters
        self.ODOM_ROTATION_NOISE = 0.01       # Odometry model rotation noise
        self.ODOM_TRANSLATION_NOISE = 0.01      # Odometry x axis (forward) noise
        self.ODOM_DRIFT_NOISE = 0.01          # Odometry y axis (side-side) noise
        # ----- Sensor model parameters
        self.NUMBER_PREDICTED_READINGS = 20     # Number of readings to predict

        self.NUMBERR_PARTICLE = 1000            #particles number
        self.NUMBERR_PARTICLE_ORIGINAL = 1000    #particles number
        self.NUMBERR_PARTICLE_MAX = 10000

        self.genNewParticle_Flat = 0

        self.timeStart = 0
        self.timeEnd = 0

        self.map_xy = list()

        self.genNewParticle_Flat = 0

        self.timeStart = 0
        self.timeEnd = 0

        self.kld = amcl_kld.KLD_sampling()
       
    def initialise_particle_cloud(self, initialpose):
        """
        Set particle cloud to initialpose plus noise

        Called whenever an initialpose message is received (to change the
        starting location of the robot), or a new occupancy_map is received.
        self.particlecloud can be initialised here. Initial pose of the robot
        is also set here.
        
        :Args:
            | initialpose: the initial pose estimate
        :Return:
            | (geometry_msgs.msg.PoseArray) poses of the particles
        """

        #find map's coordinate
        self.map_xy = find_map_coords(self.occupancy_map)
        
        #mark the start time
        self.timeStart = time.time()

        self.sortWeight = list()

        #initialise the particles
        PArray = PoseArray()
        #PArray.poses = GenerateParticle_init(self.NUMBERR_PARTICLE,initialpose, \
        #                                    self.ODOM_TRANSLATION_NOISE, self.ODOM_DRIFT_NOISE)
        PArray.poses = GenerateParticle(self.NUMBERR_PARTICLE, self.map_xy)

        

        return PArray
 
    def update_particle_cloud(self, scan):
        """
        This should use the supplied laser scan to update the current
        particle cloud. i.e. self.particlecloud should be updated.
        
        :Args:
            | scan (sensor_msgs.msg.LaserScan): laser scan to use for update

         """
        scan.ranges = ma.masked_invalid(scan.ranges).filled(scan.range_max)
        
        #initialise the variable
        weightList = list()
        newPoseList = list()
        l_weight = list()
        largestNum = 0
        kld_nx = 0
     
        #------------------------------------------------------------------------
        #use RouletteWheel algorithm to filter the particles
        #------------------------------------------------------------------------
        
        self.kld.resetKLD()
        #while True:
        newPoseList = list()           
        weight_l = list()
        weight = [0,0]
        for i in range(len(self.particlecloud.poses)):
            weight = [self.sensor_model.get_weight(scan,self.particlecloud.poses[i]),i]
            weight_l.append(weight)
            largestNum += weight[0]
            l_weight.append(largestNum)
        self.sortWeight = sorted(weight_l)
    
        while True:
            luck_num = np.random.uniform(0,largestNum)
            for i in range(len(self.particlecloud.poses)):
                source_pose = self.particlecloud.poses[i]
                if luck_num <= l_weight[i]:
                    newPoseList.append(UpdateParticle_noise(source_pose,\
                                                    self.ODOM_TRANSLATION_NOISE,\
                                                    self.ODOM_DRIFT_NOISE)) 
                    break
            if len(newPoseList) >= self.NUMBERR_PARTICLE:
                break

       
        self.particlecloud.poses = newPoseList
        print("updata particles number: " + str(len(self.particlecloud.poses)))
        
        for i in range(len(self.particlecloud.poses)):
            self.kld.find_empty_bin(self.particlecloud.poses[i].position.x*10, \
                            self.particlecloud.poses[i].position.y*10, \
                            math.degrees(getHeading(self.particlecloud.poses[i].orientation)))            

        kld_mx = self.kld.kld_sampling_algorithm()
        print("kld_mx: " + str(kld_mx))

        #self.NUMBERR_PARTICLE += 5

        if self.NUMBERR_PARTICLE >= kld_mx or self.NUMBERR_PARTICLE >= self.NUMBERR_PARTICLE_MAX:
            print("NUMBERR_PARTICLE >= kld_mx!")
            self.NUMBERR_PARTICLE = kld_mx
        else:
            print("NUMBERR_PARTICLE < kld_mx!")
            self.NUMBERR_PARTICLE += 5
        #    break
        
        #------------------------------------------------------------------------
        #update particle cloud
        

        #self.particlecloud.poses = newPoseList
        
    def estimate_pose(self):
        """
        This should calculate and return an updated robot pose estimate based
        on the particle cloud (self.particlecloud).
        
        Create new estimated pose, given particle cloud
        E.g. just average the location and orientation values of each of
        the particles and return this.
        
        Better approximations could be made by doing some simple clustering,
        e.g. taking the average location of half the particles after 
        throwing away any which are outliers

        :Return:
            | (geometry_msgs.msg.Pose) robot's estimated pose.
         """

        #------------------------------------------------------------------
        #estimate the location by the average of the position and orientation 
        #------------------------------------------------------------------ 
        x = 0
        y = 0
        z = 0
        w = 0
        k = 6 # group to 6 clusters
        pose = Pose()

        pose_cluster = kmeans_clustering(self.particlecloud.poses, k)

        for particle in pose_cluster:
            x += particle.position.x
            y += particle.position.y
            z += particle.orientation.z
            w += particle.orientation.w
        
        count = len(pose_cluster)
        pose.position.x = x/count
        pose.position.y = y/count
        pose.orientation.z = z/count
        pose.orientation.w = w/count

        return pose 

#------------------------------------------------------------------
# generate particles randomly in the coordinate of the map
#------------------------------------------------------------------
def GenerateParticle(num, map_xy):
    if num is not 0:
        poses_list = list()

        for i in range(num):
            poses_list.append(Pose())

            j = random.randint(0, len(map_xy)-1)
            poses_list[i].position.x = map_xy[j][0]
            poses_list[i].position.y = map_xy[j][1]
            poses_list[i].orientation = rotateQuaternion(Quaternion(w = 1),math.radians(random.uniform(0, 360)))
        return poses_list
    else:
        newpose = Pose()
        j = random.randint(0, len(map_xy)-1)
        newpose.position.x = map_xy[j][0]
        newpose.position.y = map_xy[j][1]
        newpose.orientation.w = 1
        newpose.orientation = rotateQuaternion(newpose.orientation,math.radians(random.uniform(0, 360)))
        return newpose

#------------------------------------------------------------------
# generate particles randomly in the coordinate of the map
#------------------------------------------------------------------
def GenerateParticle_init(num, init_pose, x_noise, y_noise):
   
    poses_list = list()
    
    for i in range(num):
        poses_list.append(Pose())
        poses_list[i].position.x = random.gauss(init_pose.pose.pose.position.x, x_noise)
        poses_list[i].position.y = random.gauss(init_pose.pose.pose.position.y, y_noise)
        heading = getHeading(init_pose.pose.pose.orientation)
        init_pose.pose.pose.orientation.w = 1
        poses_list[i].orientation = rotateQuaternion(init_pose.pose.pose.orientation,random.vonmisesvariate(heading, 35))

    return poses_list


#------------------------------------------------------------------
# add noise to the particles
#------------------------------------------------------------------
def UpdateParticle_noise(original_pose,x_noise,y_noise):
    newPose = Pose()
    newPose.position.x = random.gauss(original_pose.position.x, original_pose.position.x*x_noise)
    newPose.position.y = random.gauss(original_pose.position.y, original_pose.position.y*y_noise)

    heading = getHeading(original_pose.orientation)
    #newPose.orientation = rotateQuaternion(original_pose.orientation, \
    #                                heading+random.uniform(math.radians(-0.1),math.radians(0.1)))
    randomHeading = heading+random.uniform(math.radians(-35), math.radians(35))

    x = Quaternion()
    x.w = 1
    newPose.orientation = rotateQuaternion(x, randomHeading)
    #newPose.orientation = original_pose.orientation
    return newPose


#------------------------------------------------------------------
# find map coordinate
#------------------------------------------------------------------
def find_map_coords(occupancy_map):
    """
    Finds the coordinates of all points that are inside the actual map area

    :param occupancy_map: (nav_msgs.OccupancyGrid) world map information
    :return: coord_list: (Array of float tuples) set of pairs of coordinates
    """
    map_data = occupancy_map.data
    size = occupancy_map.info.width
    origin = occupancy_map.info.origin
    resolution = occupancy_map.info.resolution

    # Find indices of all points inside the actual map
    found_indices = [i for i, map_data in enumerate(map_data) if map_data is not -1]
    # Create an array containing them
    found_indices = np.array(found_indices)
    # Reduce the values to coordinates inside a size x size matrix
    actual_indices = found_indices % size
    # Compute two arrays containing X-axis and Y-axis indices for each point inside the map
    x_coords = ((actual_indices + 1) * resolution) + origin.position.x
    y_coords = ((found_indices - actual_indices) + size) / size * resolution + origin.position.y

    # Create a list of tuples (x, y)
    coord_list = zip(x_coords, y_coords)
    return coord_list

#------------------------------------------------------------------
# Estimate by kmean clustering
#------------------------------------------------------------------
def calc_labeled_points(poses_points, mu, poses):
    cluster = {}
    pose_clusters = {}
    idx = 0
    
    #print(ic, tcx[0], tcx[1]) #(0, array([8.46469286, 9.31281411]),(1, array([24.21137043, 24.42774952])...
    #find distance from a point to a centroid
    for pose in poses_points:
        dlist = []
        for ic, el in enumerate(mu):
            #print("=======>IC<========")
            #print(ic, el)
            tcx = mu[ic]

            #print("==========poses_points[i][0]=======")
            #print(pose, pose[0])
            dx = pose[0] - tcx[0]
            dy = pose[1] - tcx[1]
            dlist.append(math.sqrt(dx**2 + dy**2))
        #print("======dlist")
        #print(dlist)
        mind = min(dlist)
        #print("======mind>>>>")
        #print(mind)
        min_id = dlist.index(mind)
        #print("======min_id>>>>")
        #print(min_id)
        #divide into k cluster group
        try:
            cluster[min_id].append(pose)
            pose_clusters[min_id].append(poses[idx])
        except KeyError:
            cluster[min_id] = [pose]
            pose_clusters[min_id] = [poses[idx]]
        idx += 1

        #print("======pose_clusters=======")
        #print(pose_clusters)

    return cluster, pose_clusters

def find_bigger_cluster(poses_points, mu, poses):
    clusters = {}
    #find the centroid and group the points to k cluster
    (clusters, pose_clusters) = calc_labeled_points(poses_points, mu,poses)

    # Get the cluster containing the biggest number of poses
    max_len = 0
    big_cluster = []
    for key in pose_clusters.keys():
        #print("---------key------------------")
        #print(key)
        #print(len(pose_clusters[key]))
        if max_len < len(pose_clusters[key]):
            max_len = len(pose_clusters[key])
            big_cluster = pose_clusters[key]

    return big_cluster

def kmeans_clustering(poses, k):
    print(" === kmeans_clustering start=== ")
    
    #Set cluster array
    poses_points = []
    for pose in poses: 
        poses_points.extend(np.array([(pose.position.x, pose.position.y)]))
    #[array([18.62028134, 14.95356064]), array([21.4694089, 16.1901933]), array([20.7178781, 16.0949619]), array([ 6.94183776, 12.20275834]), array([25.01416455, 19.68945111]), array([ 7.07081229, 11.82859733]), array([16.65395622, 13.72024057]), array([ 7.10309018, 11.79707078]),
    #poses_points = np.array([(pose.position.x, pose.position.y) for pose in poses])
    #print("============poses_points===================")
    #print(poses_points)

    #Set random k point rom pose #cx,cy
    mu = random.sample(poses_points, k) #four of cx,cy,cz,cw
    #print("=====mu=====")
    #print(mu)

    #Return the bigger cluster
    clusters = {}
    clusters = find_bigger_cluster(poses_points, mu, poses)
    
    return clusters
