class KLD_sampling:
	def __init__(self):
		self.bins = list()
		self.nx = 0
		self.nxMin = 5000

		self.k = 1
		self.boundaryE = 0.015
		self.quantileZ = 0.504 #0.8389

		self.xInterval = 5
		self.yInterval = 5
		self.degInterval = 15
			

	def resetKLD(self):
		self.k = 1
		self.nx = 0
		self.bins = list()

	def find_empty_bin(self, x, y, deg):
		flag_find = 0
		temp = list()
		
		if len(self.bins) is not 0:
			xFlag = 0
			yFlag = 0
			degFlag = 0

			intervalx = abs(x) % self.xInterval
			intervaly = abs(y) % self.yInterval
			intervaldeg = abs(deg) % self.degInterval

			for i in range(len(self.bins)):			
				if  intervalx < self.xInterval:
					if self.bins[i][0] == x - x % self.xInterval:
						xFlag = 1
						break
				else :
					if self.bins[i][0] == x / self.xInterval * self.xInterval:
						xFlag = 1
						break

			if xFlag == 1:
				for i in range(len(self.bins)):
					if intervaly < self.yInterval:
						if self.bins[i][1] == y - y % self.yInterval:
							yFlag = 1
							break
					else :
						if self.bins[i][1] == y / self.yInterval * self.yInterval:
							yFlag = 1
							break

			if yFlag == 1:
				for i in range(len(self.bins)):
					if intervaldeg < self.degInterval:
						if self.bins[i][2] == deg - deg % self.degInterval:
							degFlag = 1
							break
					else :
						if self.bins[i][2] == deg / self.degInterval * self.degInterval:
							degFlag = 1
							break

		if len(self.bins) == 0 or degFlag == 0:
			if abs(x) % self.xInterval < self.xInterval:
				temp.append(x - x % self.xInterval)
			else:
				temp.append(x / self.xInterval * self.xInterval)

			if abs(y) % self.yInterval < self.yInterval:
				temp.append(y - y % self.yInterval)
			else:
				temp.append(y / self.yInterval * self.yInterval)

			if abs(deg) % self.degInterval < self.degInterval:
				temp.append(deg - deg % self.degInterval)
			else:
				temp.append(deg / self.degInterval * self.degInterval)

			self.bins.append(temp)
			self.k += 1

		#print(self.bins)

	def kld_sampling_algorithm(self):
		print("k: " + str(self.k))
		self.nx = ((self.k - 1)/(2 * self.boundaryE)) * \
				(1 - (2 / (9 * (self.k - 1))) + ((2 / (9 * (self.k - 1)))**(0.5)) * self.quantileZ)**3
		print("nx: " + str(self.nx))
		return self.nx



