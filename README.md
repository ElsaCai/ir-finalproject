# Final Project 

## Topic:

1. Navigation
2. Path planning
3. Localisation
4. Face Recognition

Due: 05 Dec.

## Command line instructions

You can also upload existing files from your computer using the instructions below.
### **Git global setup**

```
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR EMAIL"
```
### **Create your workspace on your own enviroment**
1. Prepare your workspace if you need 
   http://wiki.ros.org/catkin/Tutorials/create_a_workspace 
2. Check your workspace layout if you need
   http://wiki.ros.org/catkin/workspaces 

### **Create a new repository**
1. Into your worksapce folder.
```
cd catkin_ws
```

2. Download code from gitlab.
```
git clone git@git.cs.bham.ac.uk:BERNADETTE/final-project.git src
cd src
```

3. Creat your package if you need.
   http://wiki.ros.org/ROS/Tutorials/CreatingPackage

4. Into your package folder, ex. catkin_ws/src/dt_position
```
cd dt_position
touch my_python.py #Create file
git add package_name/ or your_python_file.py #Before upload your code, you shold choose which folder or file you want.
git commit -m "add package_name" or git commit -m "my_python.py" #Write note about your code.
git push -u origin master #Upload your code to gitlab.
```

### **Update code from git (origin/master)**
```
git pull #Download and update the code into your laptop from gitlab. 
```

### **How to SSH Keys with OpenSSH on MacOS or Linux**
Refer to : https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/create-with-openssh/
